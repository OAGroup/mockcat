/**
 * Configurations
 * @author Wangtd
 */
const paths = require('./lib/util.js').paths;

module.exports = {
    debug: true,
    port: 8181,
    workDir: paths('./work'),
    mockDir: paths('./work/mock'),
    blogDir: paths('./work/blog'),
    security: {
        admins: []
    },
    mailConfig: {
        host: 'smtp.qq.com',
        secureConnection: true,
        auth: {
            user: 'admin@qq.com',
            pass: '123456'
        }
    },
    logConfig: {
        appenders: {
            'console': { type: 'console' },
            'logfile': {
                type: 'file',
                filename: './logs/stdout.log',
                maxLogSize: 10485760,
                backups: 5,
                encoding: 'utf-8'
            }
        },
        categories: {
            'default': {
                appenders: ['console', 'logfile'],
                level: 'info'
            }
        }
    }
};
