/**
 * Mock Request Router
 * @author Wangtd
 */
const express = require('express');
const router = express.Router();
const os = require('os');
const uuid = require('uuid');
const fs = require('fs');
const path = require('path');
const httpRequest = require('request');
const multipart = require('connect-multiparty');
const svgCaptcha = require('svg-captcha');
const nodemailer = require('nodemailer');
const md5 = require('md5-node');

const config = require('../config.js');
const util = require('./util.js');
const logger = require('./logger.js');
const template = require('./template.js');
const security = require('./security.js');

/*======================================== auth ========================================*/

router.get('/mock/auth/captcha', function(request, response) {
    var captcha = svgCaptcha.create({ noise: 2 });
    request.session.captcha = captcha.text;
    response.type('svg');
    response.status(200).send(captcha.data);
});

router.get('/mock/auth/kaptcha', function(request, response) {
    var params = request.query;
    var kaptcha = util.random(4);
    request.session.kaptcha = kaptcha;
    // send email
    sendMail(params.email, '[Mockcat] 注册验证码', template.render('register_kaptcha', {
        kaptcha: kaptcha
    }));
    return response.end();
});

router.get('/mock/auth/rememberme', function(request, response) {
    security.fetchRememberMe(request);
    if (request.session.user) {
        return response.json({
            email: request.session.user.email,
            role: request.session.user.role
        });
    }
    response.end('timeout');
});

router.post('/mock/auth/login', function(request, response) {
    var params = request.body;
    logger.info(request, 'User login: email=[%s]', params.email);
    var users = JSON.parse(util.readFileSync(config.workDir + '/users.json'));
    for (var i = 0; i < users.length; i++) {
        if (users[i].email == params.email) {
            if (md5(params.password + users[i].salt) == users[i].password) {
                request.session.user = users[i];
                // remember me
                var value = '', maxAge = 0;
                if (params.rememberMe) {
                    var token = md5(users[i].username + ':' + users[i].password);
                    value = new Buffer(token).toString('base64');
                    maxAge = 7 * 24* 3600 * 1000;
                }
                response.cookie('remember_me', value, { maxAge: maxAge, httpOnly: true });
                return response.json({
                    email: users[i].email,
                    role: users[i].role
                });
            }
        }
    }
    response.end('user');
});

router.post('/mock/auth/logout', function(request, response) {
    logger.info(request, 'User logout');
    if (request.session.user) {
        request.session.user = null;
        response.cookie('remember_me', '', { maxAge: 0, httpOnly: true });
    }
    response.end();
});

router.post('/mock/auth/password', function(request, response) {
    var params = request.body;
    if (!isValidCaptcha(params.captcha, request)) {
        response.end('captcha');
    } else {
        logger.info(request, 'User edit password');
        if (request.session.user) {
            tryUserLock(function() {
                var filepath = config.workDir + '/users.json',
                    users = JSON.parse(util.readFileSync(filepath));
                for (var i = 0; i < users.length; i++) {
                    if (users[i].email == request.session.user.email) {
                        if (md5(params.password + users[i].salt) == users[i].password) {
                            // update password
                            users[i].salt = uuid.v1();
                            users[i].password = md5(params.newPassword + users[i].salt);
                            users[i].modified = new Date().getTime();
                            // apply for administrator
                            if (config.security.admins.indexOf(users[i].email) != -1) {
                                users[i].role = 'admin';
                            }
                            util.writeFileSync(filepath, JSON.stringify(users, null, 2), true);
                            // clear remember me
                            response.cookie('remember_me', '', { maxAge: 0, httpOnly: true });
                            return response.end();
                        } else {
                            return response.end('password');
                        }
                    }
                }
            }, function() {
                response.end('locked');
            });
        }
        response.end('timeout');
    }
});

router.post('/mock/auth/register', function(request, response) {
    var params = request.body;
    if (!isValidCaptcha(params.kaptcha, request, 'kaptcha')) {
        response.end('kaptcha');
    } else {
        logger.info(request, 'User register: email=[%s]', params.email);
        tryUserLock(function() {
            var filepath = config.workDir + '/users.json',
                users = JSON.parse(util.readFileSync(filepath));
            for (var i = 0; i < users.length; i++) {
                if (users[i].email == params.email) {
                    return response.end('email');
                }
            }
            // create user
            var now = new Date().getTime(),
                salt = uuid.v1();
            users.push({
                email: params.email,
                password: md5(params.password + salt),
                salt: salt,
                role: 'user',
                created: now,
                modified: now
            });
            util.writeFileSync(filepath, JSON.stringify(users, null, 2), true);
            response.end();
        }, function() {
            response.end('locked');
        });
    }
});

router.post('/mock/auth/reset', function(request, response) {
    var params = request.body;
    if (!isValidCaptcha(params.captcha, request)) {
        response.end('captcha');
    } else {
        logger.info(request, 'User reset: email=[%s]', params.email);
        tryUserLock(function() {
            var filepath = config.workDir + '/users.json',
                users = JSON.parse(util.readFileSync(filepath));
            for (var i = 0; i < users.length; i++) {
                if (users[i].email == params.email) {
                    // reset password
                    var password = util.random(6);
                    users[i].salt = uuid.v1();
                    users[i].password = md5(password + users[i].salt);
                    users[i].modified = new Date().getTime();
                    util.writeFileSync(filepath, JSON.stringify(users, null, 2), true);
                    // send email
                    sendMail(params.email, '[Mockcat] 密码重置', template.render('reset_password', {
                        password: password
                    }));
                    return response.end();
                }
            }
            response.end('email');
        }, function() {
            response.end('locked');
        });
    }
});

/*======================================== mock ========================================*/

router.get('/mock/module/list', function(request, response) {
    var filedir = config.mockDir;
    var files = fs.readdirSync(filedir);
    var modules = [];
    for (var i in files) {
        var filename = files[i];
        var file = fs.statSync(filedir + '/' + filename);
        if(file.isDirectory()) {
            modules.push(filename);
        }
    }
    response.json(modules);
});

router.post('/mock/module/save', function(request, response) {
    var filepath = path.join(config.mockDir, request.body.module);
    logger.info(request, 'Create module: %s', filepath);
    if(!fs.existsSync(filepath)) {
        fs.mkdirSync(filepath);
    }
    response.json(true);
});

router.post('/mock/module/rename', function(request, response) {
    var params = request.body;
    var oldPath = path.join(config.mockDir, params.oldModule),
        newPath = path.join(config.mockDir, params.newModule);
    logger.info(request, 'Rename module: %s=>%s', oldPath, newPath);
    if(fs.existsSync(newPath)) {
        response.json(false);
    } else {
        fs.renameSync(oldPath, newPath);
        response.json(true);
    }
});

router.post('/mock/module/delete', function(request, response) {
    var filepath = path.join(config.mockDir, request.body.module);
    logger.info(request, 'Delete module: %s', filepath);
    util.deleteAllSync(filepath);
    response.json(true);
});

router.get('/mock/data/tree', function(request, response) {
    var params = request.query;
    var dirpath = path.join(config.mockDir, params.module);
    if(!fs.existsSync(dirpath)) {
        return response.json(false);
    }
    var nodes = {
        name: 'MOCK',
        open: true,
        root: true,
        leaf: false
    };
    readDirSync(dirpath, nodes);
    response.json(nodes);
});

router.post('/mock/data/save', function(request, response) {
    var params = request.body;
    if (params.type == 'folder') {
        var filepath = path.join(config.mockDir, params.module, params.folder);
        logger.info(request, 'Create directory: %s', filepath);
        if(fs.existsSync(filepath)) {
            response.json(false);
        } else {
            fs.mkdirSync(filepath);
            response.json(true);
        }
    } else if (params.type == 'file') {
        var filepath = path.join(config.mockDir, params.module, params.folder + '/' + params.file);
        logger.info(request, 'Update file: path=%s, size=%d', filepath, params.data.length);
        if(params.created && fs.existsSync(filepath)) {
            response.json(false);
        } else {
            var modified = null;
            if (fs.existsSync(filepath)) {
                modified = fs.statSync(filepath).mtime.getTime();
                if (modified != params.modified) {
                    return response.end('expired');
                }
            }
            util.writeFileSync(filepath, params.data, true);
            // update modified
            modified = fs.statSync(filepath).mtime.getTime();
            response.json(modified);
        }
    }
});

router.post('/mock/data/rename', function(request, response) {
    var params = request.body;
    var oldPath = path.join(config.mockDir, params.module, params.oldPath),
        newPath = path.join(config.mockDir, params.module, params.newPath);
    logger.info(request, 'Rename path: %s=>%s', oldPath, newPath);
    if(fs.existsSync(newPath)) {
        response.json(false);
    } else {
        fs.renameSync(oldPath, newPath);
        response.json(true);
    }
});

router.post('/mock/data/delete', function(request, response) {
    var params = request.body;
    var filepath = path.join(config.mockDir, params.module, params.path);
    logger.info(request, 'Delete path: %s', filepath);
    util.deleteAllSync(filepath);
    response.json(true);
});

router.post('/mock/data/copy', function(request, response) {
    var params = request.body;
    var source = path.join(config.mockDir, params.module, params.source);
    var target = path.join(config.mockDir, params.module, params.target);
    // copy
    if (params.type == 'folder') {
        logger.info(request, 'Copy directory: %s=>%s', source, target);
        params.ignore.root = true;
        copyFolder(source, target, params.ignore);
    } else if (params.type == 'file') {
        logger.info(request, 'Copy file: %s=>%s', source, target);
        var filepath = path.dirname(target);
        if(!fs.existsSync(filepath)) {
            util.mkdirsSync(filepath);
        }
        copyFile(source, target, params.ignore);
    }
    // delete
    if (params.movable) {
        logger.info(request, 'Delete path: %s', source);
        util.deleteAllSync(source);
    }
    response.json(true);
});

router.post('/mock/blog/upload', multipart(), function(request, response) {
    var filename = '';
    if (request.files) {
        var file = request.files['file'];
        filename = uuid.v1() + path.extname(file.name);
        var filepath = path.join(config.blogDir, 'images', filename);
        // copy file
        fs.createReadStream(file.path).pipe(fs.createWriteStream(filepath));
        // delete file
        fs.unlinkSync(file.path);
    }
    response.end(filename);
});

router.post('/mock/blog/category', function(request, response) {
    tryBlogLock(function() {
        var filepath = config.blogDir + '/blogs.json',
            blogs = JSON.parse(util.readFileSync(filepath)),
            categories = blogs.categories;
        var category = null;
        // check category
        if (util.find(categories, function(category) {
            return category.id != request.body.id && category.name == request.body.category;
        })) {
            return response.end('existed');
        }
        if (request.body.id) {
            // update
            for (var i = 0; i < categories.length; i++) {
                if (categories[i].id == request.body.id) {
                    category = categories[i];
                    categories[i].name = request.body.category;
                    break;
                }
            }
        } else {
            // insert
            category = {
                id: new Date().getTime() + '',
                name: request.body.category
            };
            categories.push(category);
        }
        // order
        if (categories.indexOf(category) != request.body.order) {
            var ordered = false, orderedCategories = [];
            for (var i = 0; i < categories.length; i++) {
                if (i == request.body.order) {
                    ordered = true;
                    orderedCategories.push(category);
                }
                if (categories[i].id != category.id) {
                    orderedCategories.push(categories[i]);
                }
            }
            if (!ordered) {
                orderedCategories.push(category);
            }
            blogs.categories = orderedCategories;
        }
        // update category
        util.writeFileSync(filepath, JSON.stringify(blogs, null, 2), true);
        // response category
        response.json(category);
    }, function() {
        response.end('locked');
    });
});

router.post('/mock/blog/publish', function(request, response) {
    tryBlogLock(function() {
        var blogid = request.body.blogid,
            blog = null,
            category = null;
        var blogs = JSON.parse(util.readFileSync(config.blogDir + '/blogs.json'));
        // category
        if (request.body.category) {
            category = request.body.category;
        } else {
            var oCategory = util.find(blogs.categories, function(category) {
                return category.name == request.body.categoryName;
            });
            if (oCategory) {
                category = oCategory.id;
            } else {
                category = new Date().getTime() + '';
                blogs.categories.push({
                    id: category,
                    name: request.body.categoryName
                });
            }
        }
        // article
        if (blogid) {
            for (var i = 0; i < blogs.articles.length; i++) {
                if (blogs.articles[i].id == blogid) {
                    blog = blogs.articles[i];
                    if (request.body.modified !== blog.modified) {
                        return response.end('expired');
                    }
                    // update blog
                    blogs.articles[i].title = request.body.title;
                    blogs.articles[i].category = category;
                    blogs.articles[i].tags = request.body.tags;
                    blogs.articles[i].snapshot = request.body.snapshot;
                    blogs.articles[i].modifier = request.session.user.email;
                    blogs.articles[i].modified = new Date().getTime();
                    break;
                }
            }
        }
        if (blog == null) {
            var oBlog = util.find(blogs.articles, function(article) {
                return article.title == request.body.title;
            });
            if (oBlog) {
                return response.end('existed');
            } else {
                var now = new Date().getTime();
                blog = {
                    id: uuid.v1(),
                    title: request.body.title,
                    category: category,
                    tags: request.body.tags,
                    snapshot: request.body.snapshot,
                    creator: request.session.user.email,
                    created: now,
                    modifier: request.session.user.email,
                    modified: now
                };
                blogs.articles.unshift(blog);
            }
        }
        logger.info(request, 'Publish blog: id=%s, size=%d', blog.id, request.body.content.length);
        // update blogs
        util.writeFileSync(config.blogDir + '/blogs.json', JSON.stringify(blogs, null, 2), true);
        // update article
        util.writeFileSync(config.blogDir + '/articles/' + blog.id + '.md', request.body.content, true);
        // response blog
        response.json(blog);
    }, function() {
        response.end('locked');
    });
});

router.post('/mock/blog/delete', function(request, response) {
    tryBlogLock(function() {
        var blogs = JSON.parse(util.readFileSync(config.blogDir + '/blogs.json'));
        // category
        if (request.body.category) {
            // check category
            var oArticle = util.find(blogs.articles, function(article) {
                return article.category == request.body.category;
            });
            if (oArticle) {
                return response.end('used');
            }
            // delete category
            for (var i = 0; i < blogs.categories.length; i++) {
                if (blogs.categories[i].id == request.body.category) {
                    blogs.categories.splice(i, 1);
                    break;
                }
            }
        }
        // article
        if (request.body.article) {
            for (var i = 0; i < blogs.articles.length; i++) {
                if (blogs.articles[i].id == request.body.article) {
                    logger.info(request, 'Delete blog: ID[%s]', blogs.articles[i].id);
                    blogs.articles.splice(i, 1);
                    break;
                }
            }
        }
        // update blogs
        util.writeFileSync(config.blogDir + '/blogs.json', JSON.stringify(blogs, null, 2), true);
        response.json(true);
    }, function() {
        response.end('locked');
    });
});

/*======================================== tool ========================================*/

router.post('/mock/file/read', multipart(), function(request, response) {
    var content = '';
    if (request.files) {
        var file = request.files['file'];
        content = util.readFileSync(file.path);
        // delete file
        fs.unlinkSync(file.path);
    }
    response.writeHead(200, {
        'content-type': 'text/plain;charset=utf-8'
    });
    response.end(content);
});

router.post('/mock/file/write', function(request, response) {
    var filename = request.body.filename,
        filepath = path.join(os.tmpdir(), uuid.v1() + path.extname(filename));
    // save file
    util.writeFileSync(filepath, request.body.content, true);
    response.writeHead(200, {
        'content-type': 'application/octet-stream',
        'content-disposition': 'attachment;filename=' + encodeURI(filename)
    });
    // response stream
    fs.createReadStream(filepath, 'utf-8').on('end', function() {
        fs.unlinkSync(filepath);
    }).pipe(response);
});

router.post('/mock/test/send', function(request, response) {
    var params = request.body;
    var options = {
        url: params.url,
        method: params.method,
        headers: {
            'encoding': 'UTF-8',
            'Connection': 'keep-alive',
            'Content-Type': 'application/' + params.type,
            'Cookie': request.headers.cookie
        },
        timeout: 30000 // 30s
    };
    // headers
    if (params.headers) {
        for (var name in params.headers) {
            options.headers[name] = params.headers[name];
        }
    }
    // body
    if (params.data != null) {
        options['body'] = params.data;
    }
    httpRequest(options).on('error', function(error) {
        console.log(error);
        return response.end('The server has disconnected or the connection has timed out.');
    }).pipe(response);
});

router.post('/mock/test/request', multipart(), function(request, response) {
    var url = request.body['__url'],
        method = request.body['__method'],
        headers = JSON.parse(request.body['__headers']),
        cookies = JSON.parse(request.body['__cookies']),
        body = request.body['__body'];

    var options = {
        url: url,
        method: method,
        headers: {
            'encoding': 'UTF-8',
            'Connection': 'keep-alive',
             // cookies
            'Cookie': reformCookies(request.headers.cookie, cookies)
        },
        timeout: 900000 // 15minutes
    };

    // headers
    for (var name in headers) {
        options.headers[name] = headers[name];
    }

    // body
    if (method != 'GET' && method != 'HEAD') {
        switch (headers['Content-Type']) {
            case 'multipart/form-data':
                var formData = {};
                if (body) {
                    var fields = JSON.parse(body);
                    for (var name in fields) {
                        formData[name] = fields[name];
                    }
                }
                if (request.files) {
                    for (var name in request.files) {
                        var file = request.files[name];
                        formData[name] = {
                            value: fs.createReadStream(file.path),
                            options: {
                                filename: file.originalFilename,
                                contentType: file.type
                            }
                        };
                    }
                }
                options['formData'] = formData;
                break;
            case 'application/json':
                options['json'] = true;
                if (body) {
	                options['body'] = JSON.parse(body);
                }
                break;
            case 'application/x-www-form-urlencoded':
                if (body) {
	                options['form'] = JSON.parse(body);
                }
                break;
            case 'text/plain':
            case 'application/javascript':
            case 'application/xml':
            case 'text/xml':
            case 'text/html':
                options['body'] = body;
                break;
            default:
                options['body'] = body;
        }
    }

    httpRequest(options).on('error', function(error) {
        console.log(error);
        response.writeHead(504, {
            'content-type': 'text/plain;charset=utf-8'
        });
        return response.end('The server has disconnected or the connection has timed out.');
    }).on('response', function(response) {
        // set cookies
        response.headers['x-set-cookies'] = (response.headers['set-cookie'] || []).join('@');
    }).on('end', function() {
        // delete files
        if (request.files) {
            for (var name in request.files) {
                fs.unlinkSync(request.files[name].path);
            }
        }
    }).pipe(response);
});

function reformCookies(cookie, cookies) {
    if (Object.keys(cookies).length == 0) {
        return cookie;
    } else {
        var pattern = /([^=]+)=([^;]+);?\s*/g,
            result = null;
        while ((result = pattern.exec(cookie)) != null) {
            cookies[result[1]] = result[2];
        }
        // cookies to string
        var values = [];
        for (var name in cookies) {
            values.push(name + '=' + cookies[name]);
        }
        return values.join('; ');
    }
}

function copyFolder(source, target, ignore) {
    var copyable = ignore.root || ignore.folder;
    if (!fs.existsSync(target)) {
        copyable = true;
        util.mkdirsSync(target);
    }
    if (copyable) {
        ignore.root = false;
        var files = fs.readdirSync(source);
        for (var i in files) {
            var filename = files[i];
            var curpath = source + '/' + filename;
            var tgtpath = target + '/' + filename;
            if(fs.statSync(curpath).isDirectory()) {
                copyFolder(curpath, tgtpath, ignore);
            } else {
                copyFile(curpath, tgtpath, ignore);
            }
        }
    }
}

function copyFile(source, target, ignore) {
    if (!fs.existsSync(target) || ignore.file) {
        //fs.createReadStream(source).pipe(fs.createWriteStream(target));
        util.copyFileSync(target, source, true);
    }
}

function readDirSync(filedir, srcnode) {
    var children = [];
    var files = fs.readdirSync(filedir);
    for (var i in files) {
        var filename = files[i];
        var subnode = {};
        subnode.name = filename;
        var file = fs.statSync(filedir + '/' + filename);
        if(file.isDirectory()) {
            subnode.leaf = false;
            readDirSync(filedir + '/' + filename, subnode);
        } else {
            subnode.leaf = true;
        }
        children.push(subnode);
    }
    srcnode.children = children;
}

function isValidCaptcha(captcha, request, sessionKey) {
    if (!sessionKey) {
        sessionKey = 'captcha';
    }
    return new RegExp('^' + captcha + '$', 'i').test(request.session[sessionKey]);
}

function sendMail(account, subject, html) {
    var transport = nodemailer.createTransport(config.mailConfig);
    transport.sendMail({
        from: config.mailConfig.auth.user,
        to: account,
        subject: subject,
        html: html
    }, function(error) {
        if (error) {
            console.log(error);
        } else {
            logger.info(request, 'Send mail success: account=[%s]', account);
        }
    });
}

function tryUserLock(resolve, reject) {
    tryLock(resolve, reject, '/users.lock');
}

function tryBlogLock(resolve, reject) {
    tryLock(resolve, reject, '/blogs.lock');
}

function tryLock(resolve, reject, lock) {
    var enabled = false,
        lockpath = config.workDir + lock;
    try {
        if(fs.existsSync(lockpath)) {
            reject();
        } else {
            try {
                fs.writeFileSync(lockpath, 'locked!', { flag: 'wx' });
                enabled = true;
                resolve();
            } catch (e) {
                reject();
            }
        }
    } finally {
        if (enabled) {
            fs.unlinkSync(lockpath);
        }
    }
}

module.exports = router;
