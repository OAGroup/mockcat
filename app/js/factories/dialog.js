define([
    'angularAMD',
    'js/controllers/dialog',
    'angular-sanitize',
    'angular-ui-bootstrap'
], function(angularAMD, dialogs) {
    var dlgFactory = angular.module('app.factory.dialog', ['ngSanitize', 'ui.bootstrap']);

    dlgFactory.factory('xDialog', ['$q', '$uibModal', function($q, $uibModal) {
        var dlgFactory = {};

        dlgFactory.modal = function(options, resolve, reject) {
            var modalInstance = $uibModal.open(angular.extend({
                backdrop: 'static',
                size: 'md',
                keyboard: false
            }, options));

            var defer = $q.defer();
            modalInstance.result.then(function(data) {
                angular.isDefined(resolve) ? resolve(defer) : defer.resolve(data);
            }, function(data) {
                angular.isDefined(reject) ? reject(defer) : defer.reject(data);
            });

            return defer.promise;
        }

        dlgFactory.alert = function(title, message) {
            return dlgFactory.modal({
                templateUrl: 'templates/dialog/alert.html',
                controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                    $scope.title = title;
                    $scope.message = message;
                    $scope.ok = function () {
                        $uibModalInstance.close('close');
                    };
                }]
            }, function() {});
        }

        dlgFactory.confirm = function(title, message) {
            return dlgFactory.modal({
                templateUrl: 'templates/dialog/confirm.html',
                controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                    $scope.title = title;
                    $scope.message = message;
                    $scope.ok = function () {
                        $uibModalInstance.close('close');
                    };
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }]
            }, function(defer) {
                defer.resolve(true);
            }, function(defer) {
                defer.resolve(false);
            });
        }

        var modalInstances = {};
        angular.forEach(dialogs, function(dialogConfigs) {
            angular.forEach(dialogConfigs, function(dialogConfig) {
                modalInstances[dialogConfig.method] = function(context) {
                    return dlgFactory.modal(angularAMD.route(angular.extend({
                        controllerAs: 'dvm',
                        resolve: {
                            xContext: function () {
                                return context;
                            }
                        }
                    }, dialogConfig)));
                };
            });
        });

        dlgFactory.open = function() {
            return modalInstances;
        }

        return dlgFactory;
    }])
    .run(['$templateCache', function($templateCache) {
        $templateCache.put('templates/dialog/alert.html',
                '<div class="modal-header">' +
                '  <button type="button" class="close" ng-click="ok()">&times;</button>' +
                '  <h4 class="modal-title">{{title}}</h4>' +
                '</div>' +
                '<div class="modal-body">' +
                '  <p ng-bind-html="message"></p>' +
                '</div>' +
                '<div class="modal-footer">' +
                '  <button type="button" class="btn btn-primary" ng-click="ok()" autofocus>确定</button>' +
                '</div>');
        $templateCache.put('templates/dialog/confirm.html',
                '<div class="modal-header">' +
                '  <button type="button" class="close" ng-click="cancel()">&times;</button>' +
                '  <h4 class="modal-title">{{title}}</h4>' +
                '</div>' +
                '<div class="modal-body">' +
                '  <p ng-bind-html="message"></p>' +
                '</div>' +
                '<div class="modal-footer">' +
                '  <button type="button" class="btn btn-primary" ng-click="ok()" autofocus>确定</button>' +
                '  <button type="button" class="btn btn-default" ng-click="cancel()">取消</button>' +
                '</div>');
    }]);

    return dlgFactory;
});
