define([
    'angular-animate',
    'angular-ui-bootstrap',
    'ng-prettyjson',
    'ng-prettyjson-tmpl',
    'js/directives/markdown',
    'js/directives/form-table',
    'js/directives/api-params',
    'js/directives/url-select'
], function() {
    var directive = angular.module('app.directive', ['ngAnimate', 'ui.bootstrap', 'ng-showdown', 'ngPrettyJson',
        'app.directive.markdown', 'app.directive.formTable', 'app.directive.apiParams', 'app.directive.urlSelect']);

    // tooltip
    directive.config(['$uibTooltipProvider', function($uibTooltipProvider) {
        $uibTooltipProvider.options({
            animation: false,
            appendToBody: false,
            placement: 'auto',
            popupCloseDelay: 0,
            popupDelay: 0,
        });
    }]);

    // off autocomplete
    directive.directive('form', function() {
        return {
            restrict: 'E',
            link: function(scope, element) {
                element.attr('autocomplete', 'off');
            }
        };
    });

    // autofocus
    directive.directive('autofocus', function() {
        return {
            restrict: 'A',
            link: function(scope, element) {
                element[0].focus();
            }
        };
    });

    return directive;
});
