define(['js/factories/factory'], function() {
    var urlSelectDirective = angular.module('app.directive.urlSelect', ['app.factory.util']);

    urlSelectDirective.directive('urlSelect', ['$compile', '$templateCache', 'xUtil', function($compile, $templateCache,
            xUtil) {
        return {
            restrict: 'A',
            scope: {
                urlSelect: '='
            },
            link: function(scope, element) {
                element.addClass('input-group x-select');
                element.append($compile($templateCache.get('templates/url-select.html'))(scope));

                scope.urls = [];
                scope.selectUrl = function(url) {
                    scope.urlSelect = url;
                }

                var rhost = /^http(s)?:\/\/[\w-\.]+(:\d+)?/;
                element.find('input').on('change', function(e) {
                    if (scope.urlSelect) {
                        // parse host
                        var matches = scope.urlSelect.match(rhost);
                        if (!_.isEmpty(matches)) {
                            var urlHosts = xUtil.host.list();
                            var index = _.indexOf(urlHosts, matches[0]);
                            if (index == -1) {
                                urlHosts.unshift(matches[0]);
                                if (urlHosts.length > 20) {
                                    urlHosts.pop();
                                }
                                xUtil.host.store(urlHosts);
                            }
                        }
                    }
                });
                scope.$watch('urlSelect', function(newValue) {
                    if (newValue != null) {
                        // parse host
                        var matches = newValue.match(rhost);
                        if (_.isEmpty(matches)) {
                            scope.urls = [];
                        } else {
                            var urlHosts = xUtil.host.list();
                            var index = _.indexOf(urlHosts, matches[0]);
                            if (index > 1) {
                                urlHosts.splice(index, 1);
                                urlHosts.unshift(matches[0]);
                                xUtil.host.store(urlHosts);
                            }

                            var urls = [], path = newValue.replace(rhost, '');
                            _.forEach(urlHosts, function(host) {
                                urls.push(host + path);
                            });
                            scope.urls = urls;
                        }
                    }
                });
            }
        };
    }])
    .run(['$templateCache', function($templateCache) {
        $templateCache.put('templates/url-select.html',
                '<div class="input-group-btn">' +
                '  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" ng-disabled="urls.length == 0">' +
                '    <span class="caret"></span>' +
                '  </button>' +
                '  <ul class="dropdown-menu dropdown-menu-right" ng-if="urls.length > 0">' +
                '    <li ng-repeat="url in urls" ng-class="{\'active\': url == urlSelect}"><a href="javascript:void(0)" ng-click="selectUrl(url)">{{url}}</a></li>' +
                '  </ul>' +
                '</div>');
    }]);

    return urlSelectDirective;
});