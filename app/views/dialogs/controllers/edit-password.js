define(['app'], function(app) {
    app.controller('EditPasswordController', function($http, xContext, $uibModalInstance, toastr) {
        var self = this;
        self.randomId = 0;
        self.captcha = '';
        self.oldPassword = '';
        self.password = '';
        self.cfmPassword = '';

        self.init = function() {
            // nothing
        }

        self.changeRandomId = function() {
            self.randomId = Math.random();
        }

        self.save = function(valid) {
            if (valid && self.cfmPassword == self.password) {
                $http.post('/mock/auth/password', {
                    password: self.oldPassword,
                    newPassword: self.password,
                    captcha: self.captcha
                }).then(function(response) {
                    if (response.data == 'captcha') {
                        self.changeRandomId();
                        toastr.error('验证码不正确！');
                    } else if (response.data == 'locked') {
                        toastr.warning('已有用户在编辑，请稍候再试！');
                    } else if (response.data == 'timeout') {
                        toastr.error('用户未登录或登录已过期！');
                    } else if (response.data == 'password') {
                        toastr.error('旧密码不正确！');
                    } else if (response.status == 200) {
                        $uibModalInstance.close(true);
                        toastr.success('密码修改成功！');
                    }
                });
            }
        }

        self.cancel = function() {
            $uibModalInstance.dismiss();
        }
    });
});
