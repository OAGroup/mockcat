define(['app'], function(app) {
    app.controller('AceShortcutKeyController', function(xContext, $uibModalInstance) {
        var self = this;

        self.init = function() {
            // nothing
        }

        self.close = function() {
            $uibModalInstance.close();
        }
    });
});
