define(['app'], function(app) {
    app.controller('PublishBlogController', function($scope, $http, xContext, xDialog, $uibModalInstance, toastr) {
        var self = this;

        self.blogid = '';
        self.title = '';
        self.category = '';
        self.tags = '';
        self.content = '';

        self.created = false;
        self.categories = [];
        self.categoryName = '';

        var blogs = {};
        self.exists = {
            title: false,
            category: false
        };

        self.init = function() {
            self.blogid = xContext.blogid;
            self.content = xContext.content;
            // get blogs
            $http.get('/app/blog/blogs.json').then(function(response) {
                blogs = response.data;
                self.categories = blogs.categories;
                if (self.blogid) {
                    var blog = _.find(blogs.articles, function(article) {
                        return article.id == self.blogid;
                    });
                    if (blog) {
                        self.title = blog.title;
                        self.category = blog.category;
                        self.tags = blog.tags.join(', ');
                    } else {
                        self.blogid = '';
                    }
                }
            });

            // check title
            $scope.$watch('dvm.title', function(newValue) {
                self.exists.title = false;
                if (_.find(blogs.articles, function(article) {
                    return article.id != self.blogid && article.title == newValue;
                })) {
                    self.exists.title = true;
                }
            });

            // check category
            $scope.$watch('dvm.categoryName', function(newValue) {
                self.exists.category = false;
                if (self.created) {
                    if (_.find(blogs.categories, function(category) {
                        return category.name == newValue;
                    })) {
                        self.exists.category = true;
                    }
                }
            });
        }

        self.publish = function(valid) {
            if (valid && !self.exists.title && !self.exists.category) {
                // tags
                var tags = _.map(_.filter(self.tags.split(','), function(tag) {
                        return tag.trim() !== '';
                    }), function(tag) {
                        return tag.trim();
                    });
                // publish
                $http.post('/mock/blog/publish', {
                    blogid: self.blogid,
                    title: self.title,
                    category: (self.created ? '' : self.category),
                    categoryName: self.categoryName,
                    content: self.content,
                    tags: tags,
                    snapshot: xContext.snapshot,
                    modified: xContext.modified
                }).then(function(response) {
                    if (response.data == 'locked') {
                        xDialog.alert('提示信息', '已有博客在发布，请稍候再试！');
                    } else if (response.data == 'expired') {
                        xDialog.alert('提示信息', '博客已被更新，请刷新后再发布！');
                    } else if (response.data == 'existed') {
                        xDialog.alert('提示信息', '您的博客标题已存在！');
                    } else if (response.status == 200) {
                        toastr.success('博客已发布！');
                        $uibModalInstance.close(response.data);
                    }
                });
            }
        }

        self.cancel = function() {
            $uibModalInstance.dismiss();
        }
    });
});
