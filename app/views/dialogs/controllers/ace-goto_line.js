define(['app'], function(app) {
    app.controller('AceGotoLineController', function(xContext, $uibModalInstance) {
        var self = this;
        self.line = null;
        self.maxLine = 1;

        self.init = function() {
            self.maxLine = xContext.maxLine;
        }

        self.gotoLine = function(valid) {
            if (valid) {
                $uibModalInstance.close(self.line);
            }
        }

        self.cancel = function() {
            $uibModalInstance.dismiss();
        }
    });
});
