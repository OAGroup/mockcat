<%
var primaryKeys = _.filter(table.columns, function(column) {
    return column.primaryKey;
});
var line = 0, columnLines = [''];
_.each(table.columns, function(column, i) {
    columnLines[line] += column.name;
    if (i < table.columns.length - 1) {
        columnLines[line] += ',';
        if (columnLines[line].length < 80) {
            columnLines[line] += ' ';
        } else {
            line++;
            columnLines.push('');
        }
    }
});
%>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="<%=dao.name%>">
    <sql id="Base_Column_List">
        <%=columnLines.join('\n        ')%>
    <sql>
    <sql id="Base_Where_Clause">
        <where><%_.each(table.columns, function(column) {%>
            <if test="<%=column.javaName%> != null">
                AND <%=column.name%> = #{<%=column.javaName%>,jdbcType=<%=column.jdbcType%>}
            </if><%})%>
        </where>
    <sql>
    <select id="select" resultType="<%=model.name%>">
        SELECT
        <include refid="Base_Column_List" />
        FROM <%=table.name%>
        WHERE <%=_.map(primaryKeys, function(column, i) {
            return column.name + ' = #{' + i + ',jdbcType=' + column.jdbcType + '}';
        }).join(' AND ')%>
    </select>
    <select id="selectOne" resultType="<%=model.name%>">
        SELECT
        <include refid="Base_Column_List" />
        FROM <%=table.name%>
        <if test="_parameter != null">
            <include refid="Base_Where_Clause" />
        </if>
        LIMIT 1 <!-- Only for MySQL -->
    </select>
    <select id="selectList" resultType="<%=model.name%>">
        SELECT
        <include refid="Base_Column_List" />
        FROM <%=table.name%>
        <if test="_parameter != null">
            <include refid="Base_Where_Clause" />
        </if>
    </select>
    <select id="selectAll" resultType="<%=model.name%>">
        SELECT
        <include refid="Base_Column_List" />
        FROM <%=table.name%>
    </select>
    <select id="count" resultType="long">
        SELECT COUNT(1) FROM <%=table.name%>
        <if test="_parameter != null">
            <include refid="Base_Where_Clause" />
        </if>
    </select>
    <select id="countAll" resultType="long">
        SELECT COUNT(1) FROM <%=table.name%>
    </select>
    <insert id="insert" useGeneratedKeys="true" keyProperty="<%=_.map(primaryKeys, function(column) {
            return column.name;
        }).join(',')%>">
        INSERT INTO <%=table.name%>
        <trim prefix="(" suffix=")" suffixOverrides=","><%_.each(_.difference(table.columns, primaryKeys),
            function(column) {%>
            <if test="<%=column.javaName%> != null">
                <%=column.name%>,
            </if><%})%>
        </trim>
        <trim prefix="VALUES (" suffix=")" suffixOverrides=","><%_.each(_.difference(table.columns, primaryKeys),
            function(column) {%>
            <if test="<%=column.javaName%> != null">
                #{<%=column.javaName%>,jdbcType=<%=column.jdbcType%>},
            </if><%})%>
        </trim>
    </insert>
    <update id="update">
        UPDATE <%=table.name%>
        <set><%_.each(_.difference(table.columns, primaryKeys), function(column) {%>
            <if test="<%=column.javaName%> != null">
                <%=column.name%> = #{<%=column.javaName%>,jdbcType=<%=column.jdbcType%>},
            </if><%})%>
        </set>
        WHERE <%=_.map(primaryKeys, function(column) {
            return column.name + ' = #{' + column.javaName + ',jdbcType=' + column.jdbcType + '}';
        }).join(' AND ')%>
    </update>
    <delete id="delete">
        DELETE FROM <%=table.name%>
        WHERE <%=_.map(primaryKeys, function(column, i) {
            return column.name + ' = #{' + i + ',jdbcType=' + column.jdbcType + '}';
        }).join(' AND ')%>
    </delete>
    <delete id="deleteList">
        DELETE FROM <%=table.name%>
        <if test="_parameter != null">
            <include refid="Base_Where_Clause" />
        </if>
    </delete>
    <delete id="deleteAll">
        DELETE FROM <%=table.name%>
    </delete>
</mapper>