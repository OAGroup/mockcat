define(['app', 'css!css/timeline'], function(app) {
    app.controller('BlogViewerController', function($http, $scope, $routeParams, $timeout, $anchorScroll, toastr) {
        var self = this;
        self.title = '';
        self.code = '';
        self.headers = [];
        self.selected = '';
        self.fullScreen = false;
        self.showHeaders = true;

        var blogid = $routeParams.blogid,
            anchorScrolled = true;

        self.init = function() {
            if (blogid) {
                $http.get('/app/blog/blogs.json').then(function(response) {
                    var article = _.find(response.data.articles, function(article) {
                        return article.id == blogid;
                    });
                    if (article) {
                        self.title = article.title;
                    }
                });
                $http.get('/app/blog/articles/' + blogid + '.md').then(function(response) {
                    if (response.status == 200) {
                        self.code = response.data;
                        $timeout(function() {
                            var headers = [],
                                jqHeaders = _.map(_.filter(jQuery('.x-markdown>p [id^="section-"]'), function(e) {
                                    return e.innerText.trim() !== '';
                                }), function(header) {
                                    return jQuery(header);
                                });
                            _.forEach(jqHeaders, function(header) {
                                headers.push({
                                    id: header.attr('id'),
                                    text: header.text().trim(),
                                    level: header.prop('tagName').toLowerCase()
                                });
                            });
                            self.headers = headers;
                            // sync body scroll
                            if (!_.isEmpty(jqHeaders)) {
                                $timeout(function() {
                                    syncBodyScroll(jqHeaders);
                                });
                            }
                        }, 200);
                    }
                });
            } else {
                toastr.error('您阅读的博客不存在！');
            }
        }

        function syncBodyScroll(jqHeaders) {
            jQuery('.x-page-container').scroll(function() {
                if (anchorScrolled) {
                    var scrollTop = jQuery('.x-page-container').scrollTop();
                    for (var i = 0; i < jqHeaders.length; i++) {
                        var headerTop = jqHeaders[i].position().top + jqHeaders[i].height();
                        if (headerTop >= scrollTop) {
                            var index = i + 1;
                            jQuery('.x-header-list li').each(function(j) {
                                if (j == index) {
                                    jQuery(this).addClass('x-active');
                                } else {
                                    jQuery(this).removeClass('x-active');
                                }
                            });
                            var iHeader = jQuery('.x-header-list li:eq(' + index + ')'),
                                lHeader = jQuery('.x-header-list');
                            if (iHeader.length > 0 && lHeader.length > 0) {
                                if (iHeader.position().top <= lHeader.scrollTop()) {
                                    lHeader.scrollTop(iHeader.position().top - iHeader.height()/2);
                                } else if (iHeader.position().top + iHeader.height() > lHeader.scrollTop()
                                        + lHeader.height()) {
                                    lHeader.scrollTop(iHeader.position().top + 3*iHeader.height()/2 - lHeader.height());
                                }
                            }
                            break;
                        }
                    }
                }
            });
        }

        self.callback = function(height, width) {
            setTimeout(function() {
                var jqPageContainer = jQuery('.x-page-container');
                if (jqPageContainer[0].scrollHeight > jqPageContainer.height()) {
                    jQuery('.x-header-list').css('right', '');
                    jQuery('.x-sidebar').css('right', '');
                } else {
                    jQuery('.x-header-list').css('right', '44px');
                    jQuery('.x-sidebar').css('right', '4px');
                }
            }, 250);
        }

        self.gotoTop = function() {
            jQuery('.x-page-container').scrollTop(0);
        }

        self.scrollTo = function(id, i) {
            if (self.selected != id) {
                self.selected = id;
                anchorScrolled = false;
                jQuery('.x-header-list li:eq(' + i + ')').siblings().removeClass('x-active');
                $timeout(function() {
                    $anchorScroll(id);
                    $timeout(function() {
                        anchorScrolled = true; 
                    }, 50);
                }, 50);
            }
        }
    });
});
