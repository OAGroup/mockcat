(function(angular) {
'use strict';

  angular.module('ngPrettyJson')
  .run(['$templateCache', function ($templateCache) {
    $templateCache.put('ng-prettyjson/ng-prettyjson-panel.tmpl.html',
        '<div class="x-json-panel">\n' +
        '  <div class="x-json-button">\n' +
        '    <button type="button" title="编辑" class="btn btn-primary btn-xs" ng-click="edit()" ng-show="edition && !editActivated">\n' +
        '      <i class="fa fa-pencil"></i></button><button type="button" title="返回" class="btn btn-default btn-xs" ng-click="edit()" ng-show="edition && editActivated">\n' +
        '      <i class="fa fa-reply"></i></button><button type="button" title="更新" class="btn btn-success btn-xs" ng-click="update()" ng-show="editActivated && parsable">\n' +
        '      <i class="fa fa-check"></i></button>\n' +
        '  </div>\n' +
        '  <pre class="pretty-json ace_editor ace-tm" id="{{id}}"></pre>\n' +
        '</div>');
  }]);

})(window.angular);