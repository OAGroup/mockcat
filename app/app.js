define([
    'config',
    'angularAMD',
    'js/init',
    'css!css/default'
], function(config, angularAMD) {
    var app = angular.module('app', ['app.main']);

    app.config(function($routeProvider, $httpProvider, cfpLoadingBarProvider) {
        $routeProvider.otherwise({
            redirectTo: config.defaultPath
        });

        // loading bar's settings
        cfpLoadingBarProvider.includeBar = true;
        cfpLoadingBarProvider.includeSpinner = false;

        // http interceptor
        $httpProvider.interceptors.push(function($injector, $rootScope, $cookies) {
            return {
                responseError: function(rejection) {
                    // error
                    if (rejection.status == -1) {
                        $injector.get('xDialog').alert('错误信息', '服务器已断开连接或连接超时！');
                    } else if (rejection.status == 401) {
                        $rootScope.sessionUser = {};
                        $cookies.remove('session_user');
                        $injector.get('xDialog').open().showSecurity();
                    } else if (rejection.status == 403) {
                        $injector.get('xDialog').alert('提示信息', '您没有权限，请联系管理员！');
                    } else if (rejection.status >= 400) {
                        if (!/^\/mock\/test\//.test(rejection.config.url)) {
                            $injector.get('xDialog').alert('错误信息', rejection.data);
                        }
                    }
                    return rejection;
                }
            };
        });
    });

    app.run(function($http, $rootScope, $route, $timeout, $location, $cookies, xDialog, toastr) {
        // session user
        var sessionUser = $cookies.getObject('session_user');
        if (sessionUser) {
            $rootScope.sessionUser = sessionUser;
        } else {
            $http.get('/mock/auth/rememberme').then(function(response) {
                if (response.data == 'timeout') {
                    sessionUser = {};
                } else if (response.status == 200) {
                    sessionUser = response.data;
                }
                // update session user
                $rootScope.sessionUser = sessionUser;
                if (sessionUser.email) {
                    $cookies.putObject('session_user', sessionUser);
                } else {
                    $cookies.remove('session_user');
                }
            });
        }

        // override location path
        var original = $location.path;
        $location.path = function(path, reload) {
            if (reload === false) {
                var lastRoute = $route.current;
                var un = $rootScope.$on('$locationChangeSuccess', function() {
                    $route.current = lastRoute;
                    un();
                });
            }
            return original.apply($location, [path]);
        }

        $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
            $rootScope.routeName = current.$$route.name;
        }); 

        // resize page scope
        var callbacks = {},
            resizePageScope = function() {
                var maxHeight = Math.max(jQuery(window).height(), 500) - 176,
                    maxWidth = Math.max(jQuery(window).width(), 1024);
                jQuery('.x-page-scope').css('height', maxHeight + 'px');
                jQuery('.x-page-scope .x-editor pre').css('min-height', maxHeight + 'px');
                // callback
                if (typeof(callbacks[$rootScope.routeName]) === 'function') {
                    callbacks[$rootScope.routeName](maxHeight, maxWidth);
                }
            }
        $rootScope.refresh = function(callback) {
            callbacks[$rootScope.routeName] = callback;
            $timeout(resizePageScope);
        }
        jQuery(window).bind('load resize', resizePageScope);

        // initialize menu
        $rootScope.init = function() {
            jQuery('.navbar-header, .navbar-nav>li').each(function() {
                var $this = jQuery(this);
                $this.removeClass('active');
                var href = $this.find('a').attr('href').substring(2);
                var path = $location.path() == '/' ? config.defaultPath : $location.path();
                if (path.indexOf(href) >= 0) {
                    $this.addClass('active');
                }
                // active click menu
                $this.click(function() {
                    jQuery('.navbar-header, .navbar-nav>li').removeClass('active');
                    $this.addClass('active');
                });
            });
        }

        // security
        $rootScope.login = function() {
            xDialog.open().showSecurity();
        }
        $rootScope.editPassword = function() {
            xDialog.open().editPassword($rootScope.sessionUser);
        }
        $rootScope.logout = function() {
            $http.post('/mock/auth/logout').then(function(response) {
                if (response.status == 200) {
                    $rootScope.sessionUser = {};
                    $cookies.remove('session_user');
                    toastr.success('用户退出成功！');
                }
            });
        }
    });

    return angularAMD.bootstrap(app);
});
